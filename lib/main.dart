import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/screens/aa_list.dart';
import 'package:makspay/screens/bank_otp_screen.dart';
import 'package:makspay/screens/create_account.dart';
import 'package:makspay/screens/dashboard.dart';
import 'package:makspay/screens/getloaninfo.dart';
import 'package:makspay/screens/info_shared.dart';
import 'package:makspay/screens/language_screen.dart';
import 'package:makspay/screens/loan_agreement.dart';
import 'package:makspay/screens/loan_disbursed.dart';
import 'package:makspay/screens/loan_offers.dart';
import 'package:makspay/screens/loan_process.dart';
import 'package:makspay/screens/loan_process2.dart';
import 'package:makspay/screens/loan_repayment.dart';
import 'package:makspay/screens/mgnrega_otp_screen.dart';
import 'package:makspay/screens/shareloaninfo.dart';
import 'package:makspay/screens/sign_up.dart';
import 'package:makspay/screens/splash_screen.dart';
import 'package:makspay/theme.dart';
import 'package:makspay/screens/mgnrega_account.dart';
import 'package:makspay/screens/one_money.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'generated/l10n.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/splash': (context) => SplashScreen(),
        '/language': (context) => LanguageScreen(),
        '/signup': (context) => SignUp(),
        '/mgnregaaccount': (context) => MgnregaAccount(),
        '/mgnregaotpscreen': (context) => MgnregaOtpScreen(),
        '/createaccount': (context) => CreateAccount(),
        '/bankotpscreen': (context) => BankOtpScreen(),
        '/loanprocess': (context) => LoanProcess(),
        '/aalist': (context) => AaList(),
        '/onemoney': (context) => OneMoney(),
        '/dashboard': (context) => Dashboard(),
        '/loanprocess2': (context) => LoanProcess2(),
        '/shareloaninfo': (context) => ShareLoanInfo(),
        '/infoshared': (context) => InfoShared(),
        '/getloaninfo': (context) => GetLoanInfo(),
        '/loanoffers': (context) => LoanOffers(),
        '/loanagreement': (context) => LoanAgreement(),
        '/loanrepayment': (context) => LoanRepayment(),
        '/loandisbursed': (context) => LoanDisbursed(),
      },
      debugShowCheckedModeBanner: false,
      title: 'Maks Pay Demo',
      theme: ThemeData(
        textTheme: GoogleFonts.robotoTextTheme(),
        primaryColor: MaksPayColors.primaryMaks,
        accentColor: MaksPayColors.secondaryMaks,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        S.delegate
      ],
      supportedLocales: S.delegate.supportedLocales,
      home: LanguageScreen(),
    );
  }
}
