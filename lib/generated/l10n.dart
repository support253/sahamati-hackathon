// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Some localized strings:`
  String get pageHomeListTitle {
    return Intl.message(
      'Some localized strings:',
      name: 'pageHomeListTitle',
      desc: '',
      args: [],
    );
  }

  /// `Welcome {name}`
  String pageHomeSamplePlaceholder(Object name) {
    return Intl.message(
      'Welcome $name',
      name: 'pageHomeSamplePlaceholder',
      desc: '',
      args: [name],
    );
  }

  /// `My name is {lastName}, {firstName} {lastName}`
  String pageHomeSamplePlaceholdersOrdered(Object firstName, Object lastName) {
    return Intl.message(
      'My name is $lastName, $firstName $lastName',
      name: 'pageHomeSamplePlaceholdersOrdered',
      desc: '',
      args: [firstName, lastName],
    );
  }

  /// `{howMany, plural, one{You have 1 message} other{You have {howMany} messages}}`
  String pageHomeSamplePlural(num howMany) {
    return Intl.plural(
      howMany,
      one: 'You have 1 message',
      other: 'You have $howMany messages',
      name: 'pageHomeSamplePlural',
      desc: '',
      args: [howMany],
    );
  }

  /// `Choose Your Language`
  String get choose_your_language {
    return Intl.message(
      'Choose Your Language',
      name: 'choose_your_language',
      desc: '',
      args: [],
    );
  }

  /// `Create Account`
  String get create_account {
    return Intl.message(
      'Create Account',
      name: 'create_account',
      desc: '',
      args: [],
    );
  }

  /// `Continue`
  String get c_continue {
    return Intl.message(
      'Continue',
      name: 'c_continue',
      desc: '',
      args: [],
    );
  }

  /// `Instant Loans for MGNREGA Workers`
  String get instant_loans {
    return Intl.message(
      'Instant Loans for MGNREGA Workers',
      name: 'instant_loans',
      desc: '',
      args: [],
    );
  }

  /// `{language} Language Selected`
  String language_selected(Object language) {
    return Intl.message(
      '$language Language Selected',
      name: 'language_selected',
      desc: '',
      args: [language],
    );
  }

  /// `Enter MGNREGA Job Card Number`
  String get enter_MGNREGA_job_card_number {
    return Intl.message(
      'Enter MGNREGA Job Card Number',
      name: 'enter_MGNREGA_job_card_number',
      desc: '',
      args: [],
    );
  }

  /// `Enter Job Card Number`
  String get enter_job_card_number {
    return Intl.message(
      'Enter Job Card Number',
      name: 'enter_job_card_number',
      desc: '',
      args: [],
    );
  }

  /// `Enter MGNREGA Job Card Name`
  String get enter_MGNREGA_job_card_name {
    return Intl.message(
      'Enter MGNREGA Job Card Name',
      name: 'enter_MGNREGA_job_card_name',
      desc: '',
      args: [],
    );
  }

  /// `Enter Your Name`
  String get enter_your_name {
    return Intl.message(
      'Enter Your Name',
      name: 'enter_your_name',
      desc: '',
      args: [],
    );
  }

  /// `Verify MGNREGA OTP`
  String get verify_MGNREGA_OTP {
    return Intl.message(
      'Verify MGNREGA OTP',
      name: 'verify_MGNREGA_OTP',
      desc: '',
      args: [],
    );
  }

  /// `6 Digit OTP`
  String get otp {
    return Intl.message(
      '6 Digit OTP',
      name: 'otp',
      desc: '',
      args: [],
    );
  }

  /// `Auto reading OTP`
  String get auto_reading_otp {
    return Intl.message(
      'Auto reading OTP',
      name: 'auto_reading_otp',
      desc: '',
      args: [],
    );
  }

  /// `Enter mobile number linked to your bank account`
  String get enter_mobile_number_linked_to_your_bank_account {
    return Intl.message(
      'Enter mobile number linked to your bank account',
      name: 'enter_mobile_number_linked_to_your_bank_account',
      desc: '',
      args: [],
    );
  }

  /// `Enter Mobile Number`
  String get enter_mobile_number {
    return Intl.message(
      'Enter Mobile Number',
      name: 'enter_mobile_number',
      desc: '',
      args: [],
    );
  }

  /// `GET OTP`
  String get get_otp {
    return Intl.message(
      'GET OTP',
      name: 'get_otp',
      desc: '',
      args: [],
    );
  }

  /// `Verify BANK OTP`
  String get verify_bank_OTP {
    return Intl.message(
      'Verify BANK OTP',
      name: 'verify_bank_OTP',
      desc: '',
      args: [],
    );
  }

  /// `Loan Process`
  String get loan_process {
    return Intl.message(
      'Loan Process',
      name: 'loan_process',
      desc: '',
      args: [],
    );
  }

  /// `Register with Account Aggregator`
  String get step_1 {
    return Intl.message(
      'Register with Account Aggregator',
      name: 'step_1',
      desc: '',
      args: [],
    );
  }

  /// `Give consent to share past\nFinancial Information\n& MGNREGA Wage information`
  String get step_2 {
    return Intl.message(
      'Give consent to share past\nFinancial Information\n& MGNREGA Wage information',
      name: 'step_2',
      desc: '',
      args: [],
    );
  }

  /// `Request loan offers from lenders, Select & Agree to a  loan offer`
  String get step_3 {
    return Intl.message(
      'Request loan offers from lenders, Select & Agree to a  loan offer',
      name: 'step_3',
      desc: '',
      args: [],
    );
  }

  /// `Set up UPI Auto-repayment`
  String get step_4 {
    return Intl.message(
      'Set up UPI Auto-repayment',
      name: 'step_4',
      desc: '',
      args: [],
    );
  }

  /// `Give a consent to monitor financial information till repayment`
  String get step_5 {
    return Intl.message(
      'Give a consent to monitor financial information till repayment',
      name: 'step_5',
      desc: '',
      args: [],
    );
  }

  /// `Select Account Aggregator`
  String get select_account_aggregator {
    return Intl.message(
      'Select Account Aggregator',
      name: 'select_account_aggregator',
      desc: '',
      args: [],
    );
  }

  /// `Account Aggregator`
  String get account_aggregator {
    return Intl.message(
      'Account Aggregator',
      name: 'account_aggregator',
      desc: '',
      args: [],
    );
  }

  /// `Choose an Account Aggregator to share your financial information with lenders`
  String get selection_of_account_aggregator {
    return Intl.message(
      'Choose an Account Aggregator to share your financial information with lenders',
      name: 'selection_of_account_aggregator',
      desc: '',
      args: [],
    );
  }

  /// `Dashboard`
  String get dashboard {
    return Intl.message(
      'Dashboard',
      name: 'dashboard',
      desc: '',
      args: [],
    );
  }

  /// `Linked Accounts`
  String get linked_accounts {
    return Intl.message(
      'Linked Accounts',
      name: 'linked_accounts',
      desc: '',
      args: [],
    );
  }

  /// `Deposit`
  String get deposit {
    return Intl.message(
      'Deposit',
      name: 'deposit',
      desc: '',
      args: [],
    );
  }

  /// `Account balance:`
  String get account_balance {
    return Intl.message(
      'Account balance:',
      name: 'account_balance',
      desc: '',
      args: [],
    );
  }

  /// `MGNREGA Account`
  String get MGNREGA_account {
    return Intl.message(
      'MGNREGA Account',
      name: 'MGNREGA_account',
      desc: '',
      args: [],
    );
  }

  /// `Job Card Number`
  String get job_card_number {
    return Intl.message(
      'Job Card Number',
      name: 'job_card_number',
      desc: '',
      args: [],
    );
  }

  /// `FTO Number:`
  String get FTO_no {
    return Intl.message(
      'FTO Number:',
      name: 'FTO_no',
      desc: '',
      args: [],
    );
  }

  /// `Total man days:`
  String get total_man_days {
    return Intl.message(
      'Total man days:',
      name: 'total_man_days',
      desc: '',
      args: [],
    );
  }

  /// `Amount due`
  String get amount_due {
    return Intl.message(
      'Amount due',
      name: 'amount_due',
      desc: '',
      args: [],
    );
  }

  /// `Eligible for loan`
  String get eligible_for_loan {
    return Intl.message(
      'Eligible for loan',
      name: 'eligible_for_loan',
      desc: '',
      args: [],
    );
  }

  /// `Not eligible for loan`
  String get not_eligible_for_loan {
    return Intl.message(
      'Not eligible for loan',
      name: 'not_eligible_for_loan',
      desc: '',
      args: [],
    );
  }

  /// `SHARA DATA`
  String get share_data {
    return Intl.message(
      'SHARA DATA',
      name: 'share_data',
      desc: '',
      args: [],
    );
  }

  /// `Sharing Financial information with lenders…`
  String get sharing_financial_information_with_lenders {
    return Intl.message(
      'Sharing Financial information with lenders…',
      name: 'sharing_financial_information_with_lenders',
      desc: '',
      args: [],
    );
  }

  /// `Information Shared!`
  String get information_shared {
    return Intl.message(
      'Information Shared!',
      name: 'information_shared',
      desc: '',
      args: [],
    );
  }

  /// `Online information sharing to get loan offers from lenders is complete.`
  String get online_information_sharing_is_complete {
    return Intl.message(
      'Online information sharing to get loan offers from lenders is complete.',
      name: 'online_information_sharing_is_complete',
      desc: '',
      args: [],
    );
  }

  /// `Proceed to request loan offers`
  String get proceed_to_request_loan_offer {
    return Intl.message(
      'Proceed to request loan offers',
      name: 'proceed_to_request_loan_offer',
      desc: '',
      args: [],
    );
  }

  /// `GET LOAN OFFERS`
  String get get_loan_offers {
    return Intl.message(
      'GET LOAN OFFERS',
      name: 'get_loan_offers',
      desc: '',
      args: [],
    );
  }

  /// `Getting loan offers`
  String get getting_loan_offers {
    return Intl.message(
      'Getting loan offers',
      name: 'getting_loan_offers',
      desc: '',
      args: [],
    );
  }

  /// `Loan Offers`
  String get loan_offers {
    return Intl.message(
      'Loan Offers',
      name: 'loan_offers',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get name {
    return Intl.message(
      'Name',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `MGNREGA Amount due`
  String get MGNREGA_amount_due {
    return Intl.message(
      'MGNREGA Amount due',
      name: 'MGNREGA_amount_due',
      desc: '',
      args: [],
    );
  }

  /// `Loan`
  String get loan {
    return Intl.message(
      'Loan',
      name: 'loan',
      desc: '',
      args: [],
    );
  }

  /// `p.a`
  String get p_a {
    return Intl.message(
      'p.a',
      name: 'p_a',
      desc: '',
      args: [],
    );
  }

  /// `Pay ₹ {amount} by 30th aug`
  String pay_by(Object amount) {
    return Intl.message(
      'Pay ₹ $amount by 30th aug',
      name: 'pay_by',
      desc: '',
      args: [amount],
    );
  }

  /// `Jan`
  String get jan {
    return Intl.message(
      'Jan',
      name: 'jan',
      desc: '',
      args: [],
    );
  }

  /// `Feb`
  String get feb {
    return Intl.message(
      'Feb',
      name: 'feb',
      desc: '',
      args: [],
    );
  }

  /// `Mar`
  String get mar {
    return Intl.message(
      'Mar',
      name: 'mar',
      desc: '',
      args: [],
    );
  }

  /// `Apr`
  String get apr {
    return Intl.message(
      'Apr',
      name: 'apr',
      desc: '',
      args: [],
    );
  }

  /// `May`
  String get may {
    return Intl.message(
      'May',
      name: 'may',
      desc: '',
      args: [],
    );
  }

  /// `June`
  String get jun {
    return Intl.message(
      'June',
      name: 'jun',
      desc: '',
      args: [],
    );
  }

  /// `July`
  String get july {
    return Intl.message(
      'July',
      name: 'july',
      desc: '',
      args: [],
    );
  }

  /// `Aug`
  String get aug {
    return Intl.message(
      'Aug',
      name: 'aug',
      desc: '',
      args: [],
    );
  }

  /// `Sep`
  String get sep {
    return Intl.message(
      'Sep',
      name: 'sep',
      desc: '',
      args: [],
    );
  }

  /// `Oct`
  String get oct {
    return Intl.message(
      'Oct',
      name: 'oct',
      desc: '',
      args: [],
    );
  }

  /// `Nov`
  String get nov {
    return Intl.message(
      'Nov',
      name: 'nov',
      desc: '',
      args: [],
    );
  }

  /// `Dec`
  String get dec {
    return Intl.message(
      'Dec',
      name: 'dec',
      desc: '',
      args: [],
    );
  }

  /// `Days`
  String get days {
    return Intl.message(
      'Days',
      name: 'days',
      desc: '',
      args: [],
    );
  }

  /// `Loan Agreement`
  String get loan_agreement {
    return Intl.message(
      'Loan Agreement',
      name: 'loan_agreement',
      desc: '',
      args: [],
    );
  }

  /// `Loan Details`
  String get loan_details {
    return Intl.message(
      'Loan Details',
      name: 'loan_details',
      desc: '',
      args: [],
    );
  }

  /// `Loan Amount`
  String get loan_amount {
    return Intl.message(
      'Loan Amount',
      name: 'loan_amount',
      desc: '',
      args: [],
    );
  }

  /// `Interest`
  String get interest {
    return Intl.message(
      'Interest',
      name: 'interest',
      desc: '',
      args: [],
    );
  }

  /// `Amount Payable`
  String get amount_payable {
    return Intl.message(
      'Amount Payable',
      name: 'amount_payable',
      desc: '',
      args: [],
    );
  }

  /// `Repay By`
  String get repay_by {
    return Intl.message(
      'Repay By',
      name: 'repay_by',
      desc: '',
      args: [],
    );
  }

  /// `Late Charges`
  String get late_charges {
    return Intl.message(
      'Late Charges',
      name: 'late_charges',
      desc: '',
      args: [],
    );
  }

  /// `Prepayment Penalty`
  String get prepayment_penalty {
    return Intl.message(
      'Prepayment Penalty',
      name: 'prepayment_penalty',
      desc: '',
      args: [],
    );
  }

  /// `ACCEPT LOAN`
  String get accept_loan {
    return Intl.message(
      'ACCEPT LOAN',
      name: 'accept_loan',
      desc: '',
      args: [],
    );
  }

  /// `Auto-Repayment`
  String get auto_repayment {
    return Intl.message(
      'Auto-Repayment',
      name: 'auto_repayment',
      desc: '',
      args: [],
    );
  }

  /// `Lenders will auto-deduct repayment from your deposit A/C`
  String get lenders_will_auto_deduct_repayment_from_your_deposit {
    return Intl.message(
      'Lenders will auto-deduct repayment from your deposit A/C',
      name: 'lenders_will_auto_deduct_repayment_from_your_deposit',
      desc: '',
      args: [],
    );
  }

  /// `AUTO-REPAYMENT DETAILS`
  String get auto_repayment_details {
    return Intl.message(
      'AUTO-REPAYMENT DETAILS',
      name: 'auto_repayment_details',
      desc: '',
      args: [],
    );
  }

  /// `Repayment Bank A/C`
  String get repayment_bank_account {
    return Intl.message(
      'Repayment Bank A/C',
      name: 'repayment_bank_account',
      desc: '',
      args: [],
    );
  }

  /// `Due Date`
  String get due_date {
    return Intl.message(
      'Due Date',
      name: 'due_date',
      desc: '',
      args: [],
    );
  }

  /// `Validty of Auto-repay`
  String get validity_of_auto_repay {
    return Intl.message(
      'Validty of Auto-repay',
      name: 'validity_of_auto_repay',
      desc: '',
      args: [],
    );
  }

  /// `From 30 Aug 2020\nto 03 Sep 2020`
  String get from_to {
    return Intl.message(
      'From 30 Aug 2020\nto 03 Sep 2020',
      name: 'from_to',
      desc: '',
      args: [],
    );
  }

  /// `ENTER UPI`
  String get enter_UPI {
    return Intl.message(
      'ENTER UPI',
      name: 'enter_UPI',
      desc: '',
      args: [],
    );
  }

  /// `SUBMIT`
  String get submit {
    return Intl.message(
      'SUBMIT',
      name: 'submit',
      desc: '',
      args: [],
    );
  }

  /// `Congratulations!\nLoan Disbursed to your account...`
  String get congratulations_loan_disbursed {
    return Intl.message(
      'Congratulations!\nLoan Disbursed to your account...',
      name: 'congratulations_loan_disbursed',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'te', countryCode: 'IN'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}