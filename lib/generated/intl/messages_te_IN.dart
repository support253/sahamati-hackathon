// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a te_IN locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'te_IN';

  static m0(language) => "మీరు తెలుగు భాషను ఎంచుకున్నారు";

  static m4(amount) => "30 వ ఆగస్టు నాటికి ₹ ${amount} చెల్లించండి";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "FTO_no" : MessageLookupByLibrary.simpleMessage("FTO నంబర్:"),
    "MGNREGA_account" : MessageLookupByLibrary.simpleMessage("ఉపాధి హామీ పథకం ఖాతా"),
    "MGNREGA_amount_due" : MessageLookupByLibrary.simpleMessage("ఉపాధి హామీ పథకం మొత్తం బకాయి"),
    "accept_loan" : MessageLookupByLibrary.simpleMessage("ఈ రుణాన్ని అంగీకరించండి"),
    "account_aggregator" : MessageLookupByLibrary.simpleMessage("అకౌంట్ అగ్రిగేటర్‌"),
    "account_balance" : MessageLookupByLibrary.simpleMessage("ఖాతా నిలువ"),
    "amount_due" : MessageLookupByLibrary.simpleMessage("బకాయి మొత్తం"),
    "amount_payable" : MessageLookupByLibrary.simpleMessage("చెల్లించవలసిన మొత్తం"),
    "apr" : MessageLookupByLibrary.simpleMessage("ఏప్రిల్"),
    "aug" : MessageLookupByLibrary.simpleMessage("ఆగస్టు"),
    "auto_reading_otp" : MessageLookupByLibrary.simpleMessage("స్వయంచాలకంగా OTP ని ధృవీకరిస్తోంది"),
    "auto_repayment" : MessageLookupByLibrary.simpleMessage("స్వయంచాలకంగా తిరిగి చెల్లించే ప్రక్రియ"),
    "auto_repayment_details" : MessageLookupByLibrary.simpleMessage("స్వయంచాలకంగా తిరిగి చెల్లించే వివరాలు"),
    "c_continue" : MessageLookupByLibrary.simpleMessage("కొనసాగించండి"),
    "choose_your_language" : MessageLookupByLibrary.simpleMessage("మీ భాషను ఎంచుకోండి"),
    "congratulations_loan_disbursed" : MessageLookupByLibrary.simpleMessage("అభినందనలు!\nరుణం మీ ఖాతాకు పంపిణీ చేయబడింది"),
    "create_account" : MessageLookupByLibrary.simpleMessage("ఖాతాను సృష్టించండి"),
    "dashboard" : MessageLookupByLibrary.simpleMessage("డాష్బోర్డ్"),
    "days" : MessageLookupByLibrary.simpleMessage("రోజులు"),
    "dec" : MessageLookupByLibrary.simpleMessage("డిసెంబర్"),
    "deposit" : MessageLookupByLibrary.simpleMessage("డిపాజిట్"),
    "due_date" : MessageLookupByLibrary.simpleMessage("గడువు తేది"),
    "eligible_for_loan" : MessageLookupByLibrary.simpleMessage("రుణానికి అర్హులు"),
    "enter_MGNREGA_job_card_name" : MessageLookupByLibrary.simpleMessage("ఉపాధి హామీ పథకం జాబ్ కార్డ్ పేరును నమోదు చేయండి"),
    "enter_MGNREGA_job_card_number" : MessageLookupByLibrary.simpleMessage("ఉపాధి హామీ పథకం జాబ్ కార్డ్ నెంబర్ను నమోదు చేయండి"),
    "enter_UPI" : MessageLookupByLibrary.simpleMessage("మీ UPI ను నమోదు చేయండి"),
    "enter_job_card_number" : MessageLookupByLibrary.simpleMessage("జాబ్ కార్డ్ నెంబర్"),
    "enter_mobile_number" : MessageLookupByLibrary.simpleMessage("మొబైల్ నంబర్‌"),
    "enter_mobile_number_linked_to_your_bank_account" : MessageLookupByLibrary.simpleMessage("మీ బ్యాంక్ ఖాతాకు లింక్ చేయబడిన మొబైల్ నంబర్‌ను నమోదు చేయండి"),
    "enter_your_name" : MessageLookupByLibrary.simpleMessage("మీ పేరు"),
    "feb" : MessageLookupByLibrary.simpleMessage("ఫిబ్రవరి"),
    "from_to" : MessageLookupByLibrary.simpleMessage("30 ఆగస్టు 2020 నుండి\n3 సెప్టెంబర్ 2020 వరకు"),
    "get_loan_offers" : MessageLookupByLibrary.simpleMessage("రుణ ఆఫర్లను పొందండి"),
    "get_otp" : MessageLookupByLibrary.simpleMessage("ఒటిపి పొందండి"),
    "getting_loan_offers" : MessageLookupByLibrary.simpleMessage("రుణ ఆఫర్ ప్రక్రియ"),
    "information_shared" : MessageLookupByLibrary.simpleMessage("ఆర్థిక సమాచారాన్ని పంపించాము!"),
    "instant_loans" : MessageLookupByLibrary.simpleMessage("ఉపాధి హామీ పథకం కార్మికులకు తక్షణ రుణాలు"),
    "interest" : MessageLookupByLibrary.simpleMessage("వడ్డీ"),
    "jan" : MessageLookupByLibrary.simpleMessage("జనవరి"),
    "job_card_number" : MessageLookupByLibrary.simpleMessage("జాబ్ కార్డ్ నంబర్"),
    "july" : MessageLookupByLibrary.simpleMessage("జూలై"),
    "jun" : MessageLookupByLibrary.simpleMessage("జూన్"),
    "language_selected" : m0,
    "late_charges" : MessageLookupByLibrary.simpleMessage("ఆలస్య రుసుముపై వడ్డీ"),
    "lenders_will_auto_deduct_repayment_from_your_deposit" : MessageLookupByLibrary.simpleMessage("రుణదాతలు మీ ఖాతా నుండి స్వయంచాలకంగా ఉపసంహరించుకుంటారు"),
    "linked_accounts" : MessageLookupByLibrary.simpleMessage("లింక్ చేసిన ఖాతాలు"),
    "loan" : MessageLookupByLibrary.simpleMessage("రుణం"),
    "loan_agreement" : MessageLookupByLibrary.simpleMessage("రుణ ఒప్పందం"),
    "loan_amount" : MessageLookupByLibrary.simpleMessage("రుణం విలువ"),
    "loan_details" : MessageLookupByLibrary.simpleMessage("రుణ వివరాలు"),
    "loan_offers" : MessageLookupByLibrary.simpleMessage("రుణ ఆఫర్లు"),
    "loan_process" : MessageLookupByLibrary.simpleMessage("రుణ ప్రక్రియ"),
    "mar" : MessageLookupByLibrary.simpleMessage("మార్చి"),
    "may" : MessageLookupByLibrary.simpleMessage("మే"),
    "name" : MessageLookupByLibrary.simpleMessage("పేరు"),
    "not_eligible_for_loan" : MessageLookupByLibrary.simpleMessage("రుణానికి అనర్హులు"),
    "nov" : MessageLookupByLibrary.simpleMessage("నవంబర్"),
    "oct" : MessageLookupByLibrary.simpleMessage("అక్టోబర్"),
    "online_information_sharing_is_complete" : MessageLookupByLibrary.simpleMessage("రుణదాతల నుండి రుణ ఆఫర్లను పొందడానికి ఆన్‌లైన్ సమాచారం పంపబడింది."),
    "otp" : MessageLookupByLibrary.simpleMessage("6 అంకెల ఒటిపి"),
    "p_a" : MessageLookupByLibrary.simpleMessage("స.కి"),
    "pay_by" : m4,
    "prepayment_penalty" : MessageLookupByLibrary.simpleMessage("ముందస్తు చెల్లింపు జరిమానా"),
    "proceed_to_request_loan_offer" : MessageLookupByLibrary.simpleMessage("రుణ ఆఫర్లను అభ్యర్థించడానికి కొనసాగండి"),
    "repay_by" : MessageLookupByLibrary.simpleMessage("తిరిగి చెల్లించే తేదీ"),
    "repayment_bank_account" : MessageLookupByLibrary.simpleMessage("రుణం చెల్లించే బ్యాంక్ ఖాతా"),
    "select_account_aggregator" : MessageLookupByLibrary.simpleMessage("అకౌంట్ అగ్రిగేటర్‌ను ఎంచుకోండి"),
    "selection_of_account_aggregator" : MessageLookupByLibrary.simpleMessage("మీ ఆర్థిక సమాచారాన్ని రుణదాతలతో పంచుకోవడానికి అకౌంట్ అగ్రిగేటర్‌ను ఎంచుకోండి"),
    "sep" : MessageLookupByLibrary.simpleMessage("సెప్టెంబర్"),
    "share_data" : MessageLookupByLibrary.simpleMessage("సమాచారంను భాగస్వామ్యం చేయండి"),
    "sharing_financial_information_with_lenders" : MessageLookupByLibrary.simpleMessage("రుణదాతలతో ఆర్థిక సమాచారాన్ని పంచుతున్నాము…"),
    "step_1" : MessageLookupByLibrary.simpleMessage("అకౌంట్ అగ్రిగేటర్‌తో నమోదు చేయండి"),
    "step_2" : MessageLookupByLibrary.simpleMessage("మీ బ్యాంక్ ఆర్థిక సమాచారం\n& ఉపాధి హామీ\nపథకం వేతన\nసమాచారాన్ని పంచుకోవడానికి\nసమ్మతి ఇవ్వండి"),
    "step_3" : MessageLookupByLibrary.simpleMessage("రుుణదాతల నుండి రుణ\nఆఫర్లను అభ్యర్థించండ మరియు\nరుణ ఆఫర్ను ఎంచుకోండి"),
    "step_4" : MessageLookupByLibrary.simpleMessage("యుపిఐ (UPI) ద్వారా దానంతట\nఅదే తిరిగి చెల్లించే పద్ధతి చేయండి"),
    "step_5" : MessageLookupByLibrary.simpleMessage("రుణం తిరిగి చెల్లించే వరకు ఆర్థిక\nసమాచారాన్ని పర్యవేక్షించడానికి\nసమ్మతి ఇవ్వండి"),
    "submit" : MessageLookupByLibrary.simpleMessage("వివరాలను సమర్పించండి"),
    "total_man_days" : MessageLookupByLibrary.simpleMessage("మొత్తం పని రోజులు:"),
    "validity_of_auto_repay" : MessageLookupByLibrary.simpleMessage("చెల్లుబాటు తేదీలు"),
    "verify_MGNREGA_OTP" : MessageLookupByLibrary.simpleMessage("ఉపాధి హామీ పథకం ఒటిపి ని ధృవీకరించండి"),
    "verify_bank_OTP" : MessageLookupByLibrary.simpleMessage("బ్యాంక్ ఒటిపి ని ధృవీకరించండి")
  };
}
