// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static m0(language) => "${language} Language Selected";

  static m1(name) => "Welcome ${name}";

  static m2(firstName, lastName) => "My name is ${lastName}, ${firstName} ${lastName}";

  static m3(howMany) => "${Intl.plural(howMany, one: 'You have 1 message', other: 'You have ${howMany} messages')}";

  static m4(amount) => "Pay ₹ ${amount} by 30th aug";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "FTO_no" : MessageLookupByLibrary.simpleMessage("FTO Number:"),
    "MGNREGA_account" : MessageLookupByLibrary.simpleMessage("MGNREGA Account"),
    "MGNREGA_amount_due" : MessageLookupByLibrary.simpleMessage("MGNREGA Amount due"),
    "accept_loan" : MessageLookupByLibrary.simpleMessage("ACCEPT LOAN"),
    "account_aggregator" : MessageLookupByLibrary.simpleMessage("Account Aggregator"),
    "account_balance" : MessageLookupByLibrary.simpleMessage("Account balance:"),
    "amount_due" : MessageLookupByLibrary.simpleMessage("Amount due"),
    "amount_payable" : MessageLookupByLibrary.simpleMessage("Amount Payable"),
    "apr" : MessageLookupByLibrary.simpleMessage("Apr"),
    "aug" : MessageLookupByLibrary.simpleMessage("Aug"),
    "auto_reading_otp" : MessageLookupByLibrary.simpleMessage("Auto reading OTP"),
    "auto_repayment" : MessageLookupByLibrary.simpleMessage("Auto-Repayment"),
    "auto_repayment_details" : MessageLookupByLibrary.simpleMessage("AUTO-REPAYMENT DETAILS"),
    "c_continue" : MessageLookupByLibrary.simpleMessage("Continue"),
    "choose_your_language" : MessageLookupByLibrary.simpleMessage("Choose Your Language"),
    "congratulations_loan_disbursed" : MessageLookupByLibrary.simpleMessage("Congratulations!\nLoan Disbursed to your account..."),
    "create_account" : MessageLookupByLibrary.simpleMessage("Create Account"),
    "dashboard" : MessageLookupByLibrary.simpleMessage("Dashboard"),
    "days" : MessageLookupByLibrary.simpleMessage("Days"),
    "dec" : MessageLookupByLibrary.simpleMessage("Dec"),
    "deposit" : MessageLookupByLibrary.simpleMessage("Deposit"),
    "due_date" : MessageLookupByLibrary.simpleMessage("Due Date"),
    "eligible_for_loan" : MessageLookupByLibrary.simpleMessage("Eligible for loan"),
    "enter_MGNREGA_job_card_name" : MessageLookupByLibrary.simpleMessage("Enter MGNREGA Job Card Name"),
    "enter_MGNREGA_job_card_number" : MessageLookupByLibrary.simpleMessage("Enter MGNREGA Job Card Number"),
    "enter_UPI" : MessageLookupByLibrary.simpleMessage("ENTER UPI"),
    "enter_job_card_number" : MessageLookupByLibrary.simpleMessage("Enter Job Card Number"),
    "enter_mobile_number" : MessageLookupByLibrary.simpleMessage("Enter Mobile Number"),
    "enter_mobile_number_linked_to_your_bank_account" : MessageLookupByLibrary.simpleMessage("Enter mobile number linked to your bank account"),
    "enter_your_name" : MessageLookupByLibrary.simpleMessage("Enter Your Name"),
    "feb" : MessageLookupByLibrary.simpleMessage("Feb"),
    "from_to" : MessageLookupByLibrary.simpleMessage("From 30 Aug 2020\nto 03 Sep 2020"),
    "get_loan_offers" : MessageLookupByLibrary.simpleMessage("GET LOAN OFFERS"),
    "get_otp" : MessageLookupByLibrary.simpleMessage("GET OTP"),
    "getting_loan_offers" : MessageLookupByLibrary.simpleMessage("Getting loan offers"),
    "information_shared" : MessageLookupByLibrary.simpleMessage("Information Shared!"),
    "instant_loans" : MessageLookupByLibrary.simpleMessage("Instant Loans for MGNREGA Workers"),
    "interest" : MessageLookupByLibrary.simpleMessage("Interest"),
    "jan" : MessageLookupByLibrary.simpleMessage("Jan"),
    "job_card_number" : MessageLookupByLibrary.simpleMessage("Job Card Number"),
    "july" : MessageLookupByLibrary.simpleMessage("July"),
    "jun" : MessageLookupByLibrary.simpleMessage("June"),
    "language_selected" : m0,
    "late_charges" : MessageLookupByLibrary.simpleMessage("Late Charges"),
    "lenders_will_auto_deduct_repayment_from_your_deposit" : MessageLookupByLibrary.simpleMessage("Lenders will auto-deduct repayment from your deposit A/C"),
    "linked_accounts" : MessageLookupByLibrary.simpleMessage("Linked Accounts"),
    "loan" : MessageLookupByLibrary.simpleMessage("Loan"),
    "loan_agreement" : MessageLookupByLibrary.simpleMessage("Loan Agreement"),
    "loan_amount" : MessageLookupByLibrary.simpleMessage("Loan Amount"),
    "loan_details" : MessageLookupByLibrary.simpleMessage("Loan Details"),
    "loan_offers" : MessageLookupByLibrary.simpleMessage("Loan Offers"),
    "loan_process" : MessageLookupByLibrary.simpleMessage("Loan Process"),
    "mar" : MessageLookupByLibrary.simpleMessage("Mar"),
    "may" : MessageLookupByLibrary.simpleMessage("May"),
    "name" : MessageLookupByLibrary.simpleMessage("Name"),
    "not_eligible_for_loan" : MessageLookupByLibrary.simpleMessage("Not eligible for loan"),
    "nov" : MessageLookupByLibrary.simpleMessage("Nov"),
    "oct" : MessageLookupByLibrary.simpleMessage("Oct"),
    "online_information_sharing_is_complete" : MessageLookupByLibrary.simpleMessage("Online information sharing to get loan offers from lenders is complete."),
    "otp" : MessageLookupByLibrary.simpleMessage("6 Digit OTP"),
    "p_a" : MessageLookupByLibrary.simpleMessage("p.a"),
    "pageHomeListTitle" : MessageLookupByLibrary.simpleMessage("Some localized strings:"),
    "pageHomeSamplePlaceholder" : m1,
    "pageHomeSamplePlaceholdersOrdered" : m2,
    "pageHomeSamplePlural" : m3,
    "pay_by" : m4,
    "prepayment_penalty" : MessageLookupByLibrary.simpleMessage("Prepayment Penalty"),
    "proceed_to_request_loan_offer" : MessageLookupByLibrary.simpleMessage("Proceed to request loan offers"),
    "repay_by" : MessageLookupByLibrary.simpleMessage("Repay By"),
    "repayment_bank_account" : MessageLookupByLibrary.simpleMessage("Repayment Bank A/C"),
    "select_account_aggregator" : MessageLookupByLibrary.simpleMessage("Select Account Aggregator"),
    "selection_of_account_aggregator" : MessageLookupByLibrary.simpleMessage("Choose an Account Aggregator to share your financial information with lenders"),
    "sep" : MessageLookupByLibrary.simpleMessage("Sep"),
    "share_data" : MessageLookupByLibrary.simpleMessage("SHARA DATA"),
    "sharing_financial_information_with_lenders" : MessageLookupByLibrary.simpleMessage("Sharing Financial information with lenders…"),
    "step_1" : MessageLookupByLibrary.simpleMessage("Register with Account Aggregator"),
    "step_2" : MessageLookupByLibrary.simpleMessage("Give consent to share past\nFinancial Information\n& MGNREGA Wage information"),
    "step_3" : MessageLookupByLibrary.simpleMessage("Request loan offers from lenders, Select & Agree to a  loan offer"),
    "step_4" : MessageLookupByLibrary.simpleMessage("Set up UPI Auto-repayment"),
    "step_5" : MessageLookupByLibrary.simpleMessage("Give a consent to monitor financial information till repayment"),
    "submit" : MessageLookupByLibrary.simpleMessage("SUBMIT"),
    "total_man_days" : MessageLookupByLibrary.simpleMessage("Total man days:"),
    "validity_of_auto_repay" : MessageLookupByLibrary.simpleMessage("Validty of Auto-repay"),
    "verify_MGNREGA_OTP" : MessageLookupByLibrary.simpleMessage("Verify MGNREGA OTP"),
    "verify_bank_OTP" : MessageLookupByLibrary.simpleMessage("Verify BANK OTP")
  };
}
