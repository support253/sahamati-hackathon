import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/widgets/widgets.dart';

import '../theme.dart';

class CreateAccount extends StatefulWidget {
  @override
  _CreateAccountState createState() => _CreateAccountState();
}

class _CreateAccountState extends State<CreateAccount> {
  TextEditingController mobileController;
  Widget _buildFormField(String hintText, TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40.0),
      child: TextFormField(
        controller: controller,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
            hintText: hintText,
            filled: true,
            fillColor: Colors.white,
            border: OutlineInputBorder(borderSide: BorderSide.none)),
      ),
    );
  }

  @override
  void initState() {
    mobileController = TextEditingController();
    super.initState();
    Future.delayed(Duration(seconds: 2), () {
      mobileController.text = "9999911111";
    });
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      gestures: [
        GestureType.onTap,
      ],
      child: Scaffold(
        backgroundColor: MaksPayColors.primaryMaks,
        body: KeyboardAvoider(
          autoScroll: true,
          child: Column(
            children: [
              SizedBox(height: 50),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'MAKS',
                    style:
                        GoogleFonts.roboto(color: Colors.white, fontSize: 40),
                  ),
                  SizedBox(width: 10),
                  Text('PAY',
                      style: GoogleFonts.roboto(
                          color: MaksPayColors.secondaryMaks, fontSize: 40))
                ],
              ),
              Text(
                S.of(context).create_account,
                style: GoogleFonts.roboto(color: Colors.white, fontSize: 30),
              ),
              SizedBox(height: 40),
              Text(
                S.of(context).enter_mobile_number_linked_to_your_bank_account,
                textAlign: TextAlign.center,
                style: GoogleFonts.roboto(color: Colors.white, fontSize: 20),
              ),
              SizedBox(height: 40),
              _buildFormField(
                  S.of(context).enter_mobile_number, mobileController),
              SizedBox(height: 40),
              MaksPayWidgets.buildButton(
                  context, '/bankotpscreen', S.of(context).get_otp)
            ],
          ),
        ),
      ),
    );
  }
}
