import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:makspay/theme.dart';

class Makspaybg extends StatelessWidget {
  final Widget child;

  const Makspaybg({Key key, this.child}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      gestures: [GestureType.onDoubleTap, GestureType.onTap],
      child: Scaffold(
        backgroundColor: MaksPayColors.primaryMaks,
        body: SafeArea(
          child: KeyboardAvoider(
            autoScroll: true,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 50),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'MAKS',
                      style:
                          GoogleFonts.roboto(color: Colors.white, fontSize: 40),
                    ),
                    SizedBox(width: 10),
                    Text('PAY',
                        style: GoogleFonts.roboto(
                            color: MaksPayColors.secondaryMaks, fontSize: 40))
                  ],
                ),
                child,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
