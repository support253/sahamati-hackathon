import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/screens/makspaybg.dart';
import 'package:makspay/theme.dart';
import 'package:velocity_x/velocity_x.dart';

class LoanDisbursed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Makspaybg(
        child: Container(
      height: MediaQuery.of(context).size.height / 2,
      child: Container(
        height: context.screenHeight,
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.check_circle,
                color: MaksPayColors.secondaryMaks,
                size: 100,
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                S.of(context).congratulations_loan_disbursed,
                textAlign: TextAlign.center,
                style: GoogleFonts.roboto(
                    fontSize: 30,
                    fontWeight: FontWeight.w700,
                    color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
