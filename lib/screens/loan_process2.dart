import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/theme.dart';

class LoanProcess2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MaksPayColors.primaryMaks,
      body: Column(
        children: [
          SizedBox(height: 50),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'MAKS',
                style: GoogleFonts.roboto(color: Colors.white, fontSize: 40),
              ),
              SizedBox(width: 10),
              Text('PAY',
                  style: GoogleFonts.roboto(
                      color: MaksPayColors.secondaryMaks, fontSize: 40))
            ],
          ),
          Text(
            S.of(context).loan_process,
            style: GoogleFonts.roboto(color: Colors.white, fontSize: 30),
          ),
          SizedBox(height: 10),
          Flexible(
            child: Container(
              margin: EdgeInsets.all(16),
              padding: EdgeInsets.all(12.0),
              color: Colors.white,
              child: Transform.scale(
                scale: 1,
                alignment: Alignment(-0.5, -0.3),
                child: Stepper(
                  currentStep: 4,
                  controlsBuilder: (context, {onStepCancel, onStepContinue}) =>
                      Container(),
                  steps: <Step>[
                    Step(
                      title: Text(
                        S.of(context).step_1,
                        style: GoogleFonts.roboto(fontSize: 14),
                      ),
                      content: Container(),
                      isActive: false,
                      state: StepState.complete,
                    ),
                    Step(
                      title: Text(
                        S.of(context).step_2,
                        style: GoogleFonts.roboto(fontSize: 14),
                      ),
                      content: Container(),
                      isActive: false,
                      state: StepState.complete,
                    ),
                    Step(
                      title: AutoSizeText(
                        S.of(context).step_3,
                        maxLines: 2,
                        style: GoogleFonts.roboto(fontSize: 14),
                      ),
                      content: Container(),
                      isActive: true,
                      state: StepState.editing,
                    ),
                    Step(
                      title: Text(
                        S.of(context).step_4,
                        style: GoogleFonts.roboto(fontSize: 14),
                      ),
                      content: Container(),
                      isActive: false,
                      state: StepState.disabled,
                    ),
                    Step(
                      title: Text(
                        S.of(context).step_5,
                        style: GoogleFonts.roboto(fontSize: 14),
                      ),
                      content: Container(),
                      isActive: false,
                      state: StepState.disabled,
                    ),
                  ],
                ),
              ),
            ),
          ),
//          MaksPayWidgets.buildButton(context, '/aalist', 'CONTINUE')
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: SizedBox(
              width: MediaQuery.of(context).size.width - 100,
              height: 50,
              child: RaisedButton(
                shape: StadiumBorder(),
                color: MaksPayColors.secondaryMaks,
                onPressed: () {
                  Navigator.of(context).pushNamed('/shareloaninfo');
                },
                child: Text(S.of(context).c_continue,
                    style: GoogleFonts.roboto(
                        color: MaksPayColors.primaryMaks,
                        fontWeight: FontWeight.w700)),
              ),
            ),
          )
        ],
      ),
    );
  }
}
