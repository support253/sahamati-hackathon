import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/generated/l10n.dart';

class OneMoney extends StatefulWidget {
  @override
  _OneMoneyState createState() => _OneMoneyState();
}

class _OneMoneyState extends State<OneMoney> {
  final flutterWebviewPlugin = FlutterWebviewPlugin();
// ignore: prefer_collection_literals

  @override
  void initState() {
    super.initState();
//    flutterWebviewPlugin.launch("https://aa-sandbox.onemoney.in/");
    flutterWebviewPlugin.onUrlChanged.listen((String url) {
      print(url);
      if (mounted) {
        setState(() {
//          _history.add('onUrlChanged: $url');
        });
      }
    });
//    flutterWebviewPlugin.onUrlChanged.listen((String url) {
//      print("$url");
//      if (url ==
//          "https://aa-sandbox.onemoney.in/onemoney-webapp/start-account-linking") {
//        print("concent sent!");
//      } else if (url ==
//          "https://aa-sandbox.onemoney.in/onemoney-webapp/consent") {
//        print("concent approved by user returning to app!");
//      }
//    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
            Navigator.pushNamed(context, "/dashboard");
          },
          icon: Icon(Icons.arrow_back_ios),
        ),
        title: Text(
          S.of(context).account_aggregator,
          style: GoogleFonts.roboto(color: Colors.white),
        ),
//        actions: [
//          IconButton(
//            onPressed: () {
//              Navigator.pushNamed(context, "/dashboard");
//            },
//            icon: Icon(Icons.arrow_forward_ios),
//          ),
//        ],
      ),
      body: SafeArea(
        child: WebviewScaffold(
//        javascriptChannels: jsChannels,

          appCacheEnabled: true,
          withLocalStorage: true,
          withJavascript: true,
          url: 'https://aa-sandbox.onemoney.in/',
        ),
      ),
    );
  }
}
