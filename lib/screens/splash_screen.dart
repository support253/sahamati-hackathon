import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/theme.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MaksPayColors.primaryMaks,
      body: Container(
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'MAKS',
                style: GoogleFonts.roboto(color: Colors.white, fontSize: 40),
              ),
              SizedBox(width: 10),
              Text('PAY',
                  style: GoogleFonts.roboto(
                      color: MaksPayColors.secondaryMaks, fontSize: 40))
            ],
          ),
        ),
      ),
    );
  }
}
