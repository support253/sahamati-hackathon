import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/widgets/widgets.dart';

import '../theme.dart';

class BankOtpScreen extends StatefulWidget {
  @override
  _BankOtpScreenState createState() => _BankOtpScreenState();
}

class _BankOtpScreenState extends State<BankOtpScreen> {
  TextEditingController otpController;
  int textLength = 0;

  Widget _buildFormField(String hintText, TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40.0),
      child: TextFormField(
        controller: controller,
        textAlign: TextAlign.center,
        style: TextStyle(letterSpacing: 5),
        decoration: InputDecoration(
            hintText: hintText,
            filled: true,
            fillColor: Colors.white,
            border: OutlineInputBorder(borderSide: BorderSide.none)),
      ),
    );
  }

  @override
  void initState() {
    otpController = TextEditingController();
    super.initState();
    Future.delayed(Duration(seconds: 5), () {
      otpController.text = "763201";
      setState(() {
        textLength = otpController.text.length;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      gestures: [GestureType.onTap],
      child: Scaffold(
        backgroundColor: MaksPayColors.primaryMaks,
        body: KeyboardAvoider(
          autoScroll: true,
          child: Column(
            children: [
              SizedBox(height: 50),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'MAKS',
                    style:
                        GoogleFonts.roboto(color: Colors.white, fontSize: 40),
                  ),
                  SizedBox(width: 10),
                  Text('PAY',
                      style: GoogleFonts.roboto(
                          color: MaksPayColors.secondaryMaks, fontSize: 40))
                ],
              ),
              Text(
                S.of(context).create_account,
                style: GoogleFonts.roboto(color: Colors.white, fontSize: 30),
              ),
              SizedBox(height: 40),
              Text(
                S.of(context).verify_bank_OTP,
                textAlign: TextAlign.center,
                style: GoogleFonts.roboto(color: Colors.white, fontSize: 20),
              ),
              SizedBox(height: 40),
              _buildFormField(S.of(context).otp, otpController),
              SizedBox(height: 10),
              textLength == 0
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Transform.scale(
                          scale: 0.5,
                          //TODO:change color of CircularProgressIndicator later
                          child: CircularProgressIndicator(
                            strokeWidth: 2,
                          ),
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        Text(S.of(context).auto_reading_otp,
                            style: TextStyle(color: Colors.white)),
                      ],
                    )
                  : Container(),
              SizedBox(height: 40),
              MaksPayWidgets.buildButton(
                  context, '/loanprocess', S.of(context).c_continue)
            ],
          ),
        ),
      ),
    );
  }
}
