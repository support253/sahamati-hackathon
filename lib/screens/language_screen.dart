import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/theme.dart';
import 'package:makspay/widgets/widgets.dart';
import 'package:makspay/generated/l10n.dart';

class LanguageScreen extends StatefulWidget {
  @override
  _LanguageScreenState createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  String selectedLanguage = '';

  Widget _buildLangButton(String lName) {
    return FlatButton(
        shape: StadiumBorder(),
        color: Colors.white,
        onPressed: () {
          setState(() {
            selectedLanguage = lName;
          });
          switch (lName) {
            case "English":
              S.load(Locale("en", "US"));
              break;
            case "मराठी":
              S.load(Locale("mr", "IN"));
              break;
            case "हिन्दी":
              S.load(Locale("hi", "IN"));
              break;
            case "తెలుగు":
              S.load(Locale("te", "IN"));
              break;
            case "தமிழ்":
              S.load(Locale("ta", "IN"));
              break;
            case "ગુજરાતી":
              S.load(Locale("gu", "IN"));
              break;
          }
        },
        child: Text(
          lName,
        ));
  }

  @override
  Widget build(BuildContext context) {
    // Widget _buildButton(String navRoute) {
    //   return Padding(
    //     padding: const EdgeInsets.all(12.0),
    //     child: RaisedButton(
    //       color: MaksPayColors.secondaryMaks,
    //       onPressed: () {
    //         Navigator.of(context).pushNamed(navRoute);
    //       },
    //       child: Text('CONTINUE',
    //           style: GoogleFonts.roboto(color: MaksPayColors.primaryMaks)),
    //     ),
    //   );
    // }

    return Scaffold(
        backgroundColor: MaksPayColors.primaryMaks,
        body: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 100,
            ),
            Center(
              child: Text(
                "Choose Your Language",
                style: GoogleFonts.roboto(color: Colors.white, fontSize: 24),
              ),
            ),
            Flexible(
              flex: 3,
              child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _buildLangButton('English'),
                      SizedBox(width: 25),
                      _buildLangButton('मराठी')
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _buildLangButton('हिन्दी'),
                      SizedBox(width: 25),
                      _buildLangButton('తెలుగు')
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _buildLangButton('தமிழ்'),
                      SizedBox(width: 25),
                      _buildLangButton('ગુજરાતી')
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            selectedLanguage.isEmpty
                ? Container()
                : Text(S.of(context).language_selected(selectedLanguage),
                    style:
                        GoogleFonts.roboto(color: Colors.white, fontSize: 24)),
            Spacer(),
            MaksPayWidgets.buildButton(
                context, '/signup', S.of(context).c_continue)
          ],
        ));
  }
}
