import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:makspay/generated/l10n.dart';
import 'package:makspay/theme.dart';

class GetLoanInfo extends StatefulWidget {
  @override
  _GetLoanInfoState createState() => _GetLoanInfoState();
}

class _GetLoanInfoState extends State<GetLoanInfo> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 5),
        () => Navigator.pushNamed(context, "/loanoffers"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MaksPayColors.primaryMaks,
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(
                backgroundColor: MaksPayColors.primaryMaks,
//                valueColor: Colors.white,
              ),
              SizedBox(
                height: 40,
              ),
              Text(
                S.of(context).getting_loan_offers,
                textAlign: TextAlign.center,
                style: GoogleFonts.roboto(fontSize: 20, color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
