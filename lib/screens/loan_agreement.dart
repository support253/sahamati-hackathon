import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/screens/makspaybg.dart';
import 'package:makspay/theme.dart';
import 'package:makspay/widgets/widgets.dart';

class LoanAgreement extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Makspaybg(
        child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset(
                  "assets/bank/sbi.png",
                  scale: 30,
                ),
                Text(
                  S.of(context).loan_agreement,
                  textAlign: TextAlign.left,
                  style: GoogleFonts.roboto(
                      fontSize: 35,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  width: 5,
                ),
                IconButton(
                  icon: Icon(
                    Icons.file_download,
                    color: MaksPayColors.secondaryMaks,
                  ),
                  onPressed: null,
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "${S.of(context).name}: Kavita Devi",
              textAlign: TextAlign.left,
              style: GoogleFonts.roboto(
                fontSize: 15,
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Text("${S.of(context).MGNREGA_amount_due}: ₹ 3104.00",
                textAlign: TextAlign.left,
                style: GoogleFonts.roboto(
                  fontSize: 15,
                  color: Colors.white,
                )),
            SizedBox(
              height: 25,
            ),
            Center(
              child: Text(
                S.of(context).loan_details,
                textAlign: TextAlign.center,
                style: GoogleFonts.roboto(
                    fontSize: 25,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 300,
              child: ListView(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                children: [
                  ListTile(
                    title: Text(S.of(context).loan_amount,
                        style: GoogleFonts.roboto(color: Colors.white)),
                    trailing: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Rs.2793.60',
                            style: GoogleFonts.roboto(color: Colors.white)),
                        SizedBox(height: 4),
                        Text('90 % LTV',
                            style: GoogleFonts.roboto(color: Colors.white)),
                      ],
                    ),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  ListTile(
                    title: Text(S.of(context).interest,
                        style: GoogleFonts.roboto(color: Colors.white)),
                    trailing: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Rs.391.10',
                            style: GoogleFonts.roboto(color: Colors.white)),
                        SizedBox(height: 4),
                        Text('14 % P.A',
                            style: GoogleFonts.roboto(color: Colors.white)),
                      ],
                    ),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  ListTile(
                    title: Text(S.of(context).amount_payable,
                        style: GoogleFonts.roboto(color: Colors.white)),
                    trailing: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Rs.2826.19',
                            style: GoogleFonts.roboto(color: Colors.white)),
                      ],
                    ),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  ListTile(
                    title: Text(S.of(context).repay_by,
                        style: GoogleFonts.roboto(color: Colors.white)),
                    trailing: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('30 ${S.of(context).aug} 2020',
                            style: GoogleFonts.roboto(color: Colors.white)),
                        SizedBox(height: 4),
                        Text('i.e. 30 ${S.of(context).days}',
                            style: GoogleFonts.roboto(color: Colors.white)),
                      ],
                    ),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  ListTile(
                    title: Text(S.of(context).late_charges,
                        style: GoogleFonts.roboto(color: Colors.white)),
                    trailing: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('8 % per month',
                            style: GoogleFonts.roboto(color: Colors.white)),
                      ],
                    ),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  ListTile(
                    title: Text(S.of(context).prepayment_penalty,
                        style: GoogleFonts.roboto(color: Colors.white)),
                    trailing: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Zero',
                            style: GoogleFonts.roboto(color: Colors.white)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 8,
            ),
            MaksPayWidgets.buildButton(
                context, '/loanrepayment', S.of(context).accept_loan)
          ]),
    ));
  }
}
