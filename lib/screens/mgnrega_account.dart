import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/widgets/widgets.dart';

import '../theme.dart';

class MgnregaAccount extends StatefulWidget {
  @override
  _MgnregaAccountState createState() => _MgnregaAccountState();
}

class _MgnregaAccountState extends State<MgnregaAccount> {
  TextEditingController jobController;
  TextEditingController nameController;
  Widget _buildFormField(String hintText, TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40.0),
      child: TextFormField(
        controller: controller,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
            hintText: hintText,
            filled: true,
            fillColor: Colors.white,
            border: OutlineInputBorder(borderSide: BorderSide.none)),
      ),
    );
  }

  @override
  void initState() {
    jobController = TextEditingController();
    nameController = TextEditingController();
    super.initState();
    Future.delayed(Duration(seconds: 2), () {
      jobController.text = "BH-45-005-009-02484100/381";
      nameController.text = "Kavita Devi";
      print("controller changed");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MaksPayColors.primaryMaks,
      body: ListView(
        children: [
          SizedBox(height: 50),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'MAKS',
                style: GoogleFonts.roboto(color: Colors.white, fontSize: 40),
              ),
              SizedBox(width: 10),
              Text('PAY',
                  style: GoogleFonts.roboto(
                      color: MaksPayColors.secondaryMaks, fontSize: 40))
            ],
          ),
          Text(
            S.of(context).create_account,
            textAlign: TextAlign.center,
            style: GoogleFonts.roboto(color: Colors.white, fontSize: 30),
          ),
          SizedBox(height: 40),
          Text(
            S.of(context).enter_MGNREGA_job_card_number,
            textAlign: TextAlign.center,
            style: GoogleFonts.roboto(color: Colors.white, fontSize: 20),
          ),
          SizedBox(height: 40),
//          _buildFormField('AP-05-016-014-014/010003'),
          _buildFormField(S.of(context).enter_job_card_number, jobController),
          SizedBox(height: 40),
          Text(
            S.of(context).enter_MGNREGA_job_card_name,
            textAlign: TextAlign.center,
            style: GoogleFonts.roboto(color: Colors.white, fontSize: 20),
          ),
          SizedBox(height: 40),
          _buildFormField(S.of(context).enter_your_name, nameController),
          SizedBox(height: 40),

          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: MaksPayWidgets.buildButton(
                context, '/mgnregaotpscreen', S.of(context).c_continue),
          ),
        ],
      ),
    );
  }
}
