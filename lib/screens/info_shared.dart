import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/theme.dart';
import 'package:makspay/widgets/widgets.dart';

class InfoShared extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MaksPayColors.primaryMaks,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Icon(Icons.playlist_add_check, size: 100, color: Colors.white),
          SizedBox(
            height: 20,
          ),
          Text(
            S.of(context).information_shared,
            textAlign: TextAlign.center,
            style: GoogleFonts.roboto(
                fontWeight: FontWeight.bold, fontSize: 30, color: Colors.white),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              S.of(context).online_information_sharing_is_complete,
              textAlign: TextAlign.center,
              style: GoogleFonts.roboto(fontSize: 20, color: Colors.white),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              S.of(context).proceed_to_request_loan_offer,
              textAlign: TextAlign.center,
              style: GoogleFonts.roboto(fontSize: 20, color: Colors.white),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          MaksPayWidgets.buildButton(
              context, "/getloaninfo", S.of(context).get_loan_offers)
        ],
      ),
    );
  }
}
