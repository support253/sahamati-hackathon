import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/theme.dart';

class ShareLoanInfo extends StatefulWidget {
  @override
  _ShareLoanInfoState createState() => _ShareLoanInfoState();
}

class _ShareLoanInfoState extends State<ShareLoanInfo> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 5),
        () => Navigator.pushNamed(context, "/infoshared"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MaksPayColors.primaryMaks,
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(
                backgroundColor: MaksPayColors.primaryMaks,
//                valueColor: Colors.white,
              ),
              SizedBox(
                height: 40,
              ),
              Text(
                S.of(context).sharing_financial_information_with_lenders,
                textAlign: TextAlign.center,
                style: GoogleFonts.roboto(fontSize: 20, color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
