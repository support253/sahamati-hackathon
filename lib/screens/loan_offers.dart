import 'package:makspay/widgets/loan_offers_card.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/screens/makspaybg.dart';

class LoanOffers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Makspaybg(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: 20,
            ),
            Text(
              S.of(context).loan_offers,
              textAlign: TextAlign.left,
              style: GoogleFonts.roboto(
                  fontSize: 35,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "${S.of(context).name}: Kavita Devi",
              textAlign: TextAlign.left,
              style: GoogleFonts.roboto(
                fontSize: 15,
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              "${S.of(context).MGNREGA_amount_due}: ₹ 3104.00",
              textAlign: TextAlign.left,
              style: GoogleFonts.roboto(
                fontSize: 15,
                color: Colors.white,
              ),
            ),
            ListView(
              shrinkWrap: true,
              children: [
                ///ICICI BANK
                LoanOffersCard(
                  offeredLoan: 2483.20,
                  interest: 372.48,
                  pA: 15,
                  payBy: 2514.24,
                  bankImage: "assets/bank/icici.png",
                  bankImageScale: 20,
                  bankName: "ICICI Bank",
                ),
                SizedBox(
                  height: 8,
                ),

                ///SBI BANK
                LoanOffersCard(
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      "/loanagreement",
                    );
                  },
                  offeredLoan: 2793.60,
                  interest: 391.10,
                  pA: 14,
                  payBy: 2826.19,
                  bankImage: "assets/bank/sbi.png",
                  bankImageScale: 60,
                  bankName: "SBI Bank",
                ),
                SizedBox(
                  height: 8,
                ),

                ///AXIS BANK
                LoanOffersCard(
                  offeredLoan: 2638.40,
                  interest: 448.53,
                  pA: 17,
                  payBy: 2675.78,
                  bankImage: "assets/bank/axis.png",
                  bankImageScale: 25,
                  bankName: "AXIS Bank",
                ),
                SizedBox(
                  height: 8,
                ),

                ///KOTAK BANK
                LoanOffersCard(
                  offeredLoan: 2638.40,
                  interest: 527.68,
                  pA: 20,
                  payBy: 2682.37,
                  bankImage: "assets/bank/kotak.webp",
                  bankImageScale: 25,
                  bankName: "Kotak Bank",
                ),
                SizedBox(
                  height: 8,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
