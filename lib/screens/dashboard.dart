import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/theme.dart';
import 'package:makspay/widgets/widgets.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MaksPayColors.primaryMaks,
      appBar: AppBar(
        title: Text(
          S.of(context).dashboard,
          style: GoogleFonts.roboto(color: Colors.white),
        ),
      ),
//      backgroundColor: Colors.white,
      body: ListView(
        children: [
//          ListTile(
//            title: Text("UCO Bank"),
//            subtitle: Text("XXXXXXXXXX3330"),
//          )
          Card(
            elevation: 5,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          S.of(context).linked_accounts,
                          style:
                              GoogleFonts.roboto(fontWeight: FontWeight.bold),
                        ),
                      ),
                      Row(
                        children: [
                          Image.asset(
                            "assets/images/uco-bank.png",
                            scale: 10,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("UCO Bank"),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(S.of(context).deposit),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("XXXXXXXXXX3330"),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                    "${S.of(context).account_balance}: ₹ 18.00"),
                              ),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Card(
            elevation: 5,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Image.asset(
                            "assets/images/mg.jpg",
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  S.of(context).MGNREGA_account,
                                  style: GoogleFonts.roboto(
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "${S.of(context).job_card_number} :\nBH-45-005-009-02484100/381",
                                  style: TextStyle(fontSize: 12),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "${S.of(context).FTO_no}:\nBH0545005_120720APB_FTO_289189",
                                  style: TextStyle(fontSize: 12),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                    "${S.of(context).total_man_days}: 16 days"),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                    "${S.of(context).amount_due}: ₹ 3104.00"),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Chip(
                                  label: Text(S.of(context).eligible_for_loan),
                                  labelStyle: GoogleFonts.roboto(
                                      fontStyle: FontStyle.italic,
                                      color: MaksPayColors.primaryMaks),
                                ),
                              ),
//                               Padding(
//                                 padding: const EdgeInsets.all(8.0),
//                                 child: Container(
//                                   color: MaksPayColors.secondaryMaks,
//                                   padding: EdgeInsets.all(8),
//                                   child: Text(
//                                     "Loan eligible",
//                                     style: TextStyle(
//                                       color: MaksPayColors.primaryMaks,
//                                       fontWeight: FontWeight.bold,
// //                                    fontStyle: FontStyle.italic,
//                                     ),
//                                   ),
//                                 ),
//                               ),
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          MaksPayWidgets.buildButton(
            context,
            "/loanprocess2",
            S.of(context).share_data,
          )
        ],
      ),
    );
  }
}
