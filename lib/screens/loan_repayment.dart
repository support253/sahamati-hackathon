import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/screens/makspaybg.dart';
import 'package:makspay/theme.dart';
import 'package:makspay/widgets/widgets.dart';
import 'package:velocity_x/velocity_x.dart';

class LoanRepayment extends StatefulWidget {
  @override
  _LoanRepaymentState createState() => _LoanRepaymentState();
}

class _LoanRepaymentState extends State<LoanRepayment> {
  TextEditingController upiController;

  Widget _buildFormField(String hintText, TextEditingController controller) {
    return TextFormField(
      controller: controller,
//      textAlign: TextAlign.center,
      decoration: InputDecoration(
          hintText: hintText,
          filled: true,
          fillColor: Color(0xFFF7F7F7),
          border: OutlineInputBorder()),
    );
  }

  @override
  void initState() {
    upiController = TextEditingController();
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      upiController.text = "kavitadevi@upi";
      print("controller changed");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Makspaybg(
        child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: 20,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
//                Image.asset(
//                  "assets/bank/sbi.png",
//                  scale: 34,
//                ),
                Container(
                  width: 40,
                  height: 28,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                    alignment: Alignment.center,
                    fit: BoxFit.cover,
                    image: AssetImage("assets/bank/sbi.png"),
                  )),
                ),
                Text(
                  "SBI Bank",
                  textAlign: TextAlign.left,
                  style: GoogleFonts.roboto(
                      fontSize: 25,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              S.of(context).auto_repayment,
              textAlign: TextAlign.left,
              style: GoogleFonts.roboto(
                  fontSize: 30,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "${S.of(context).lenders_will_auto_deduct_repayment_from_your_deposit} UCO ****0330",
              textAlign: TextAlign.left,
              style: GoogleFonts.roboto(
                fontSize: 18,
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              S.of(context).auto_repayment_details,
              textAlign: TextAlign.left,
              style: GoogleFonts.roboto(
                fontSize: 18,
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.white.withOpacity(0.7),
              ),
              padding: EdgeInsets.all(10),
              height: context.safePercentHeight * 22,
              width: context.screenWidth,
              child: Row(children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${S.of(context).repayment_bank_account}:',
                      style:
                          GoogleFonts.roboto(color: MaksPayColors.primaryMaks),
                    ),
                    Text(
                      '${S.of(context).amount_payable}:',
                      style:
                          GoogleFonts.roboto(color: MaksPayColors.primaryMaks),
                    ),
                    Text(
                      '${S.of(context).due_date}:',
                      style:
                          GoogleFonts.roboto(color: MaksPayColors.primaryMaks),
                    ),
                    Text(
                      '${S.of(context).validity_of_auto_repay}:',
                      style:
                          GoogleFonts.roboto(color: MaksPayColors.primaryMaks),
                    ),
                  ],
                ),
                Spacer(),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'UCO ****330',
                      style:
                          GoogleFonts.roboto(color: MaksPayColors.primaryMaks),
                    ),
                    Text(
                      'Rs. 2826.19',
                      style:
                          GoogleFonts.roboto(color: MaksPayColors.primaryMaks),
                    ),
                    Text(
                      '30 ${S.of(context).aug} 2020',
                      style:
                          GoogleFonts.roboto(color: MaksPayColors.primaryMaks),
                    ),
                    Text(
                      S.of(context).from_to,
                      style:
                          GoogleFonts.roboto(color: MaksPayColors.primaryMaks),
                    ),
                  ],
                ),
              ]),
            ),
            SizedBox(
              height: 30,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  S.of(context).enter_UPI,
                  style: GoogleFonts.roboto(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 20,
                ),
                _buildFormField('${S.of(context).name}@UPI', upiController),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: MaksPayWidgets.buildButton(
                  context, '/loandisbursed', S.of(context).submit),
            ),
          ]),
    ));
  }
}
