import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/widgets/widgets.dart';

import '../theme.dart';

class SignUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MaksPayColors.primaryMaks,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        'assets/images/mgnrega.jpg',
                      ),
                      fit: BoxFit.cover,
                    ),
                    // colorFilter: ColorFilter.mode(
                    //     Colors.black.withOpacity(0.8), BlendMode.dstATop)),
                  ),
                  child: Container(
                      height: double.maxFinite,
                      width: double.maxFinite,
                      color: Colors.black.withOpacity(0.7),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            S.of(context).instant_loans,
                            textAlign: TextAlign.center,
                            style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontSize: 30,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          MaksPayWidgets.buildButton(context, '/mgnregaaccount',
                              S.of(context).c_continue)
                        ],
                      )),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
