import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/widgets/widgets.dart';

import '../theme.dart';

class AaList extends StatefulWidget {
  @override
  _AaListState createState() => _AaListState();
}

class _AaListState extends State<AaList> {
  bool selectedAA = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MaksPayColors.primaryMaks,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(children: [
              SizedBox(height: 50),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'MAKS',
                    style:
                        GoogleFonts.roboto(color: Colors.white, fontSize: 40),
                  ),
                  SizedBox(width: 10),
                  Text('PAY',
                      style: GoogleFonts.roboto(
                          color: MaksPayColors.secondaryMaks, fontSize: 40))
                ],
              ),
              SizedBox(height: 40),
              AutoSizeText(
                S.of(context).select_account_aggregator,
                style: GoogleFonts.roboto(
                    color: Colors.white,
                    fontSize: 24,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 20),
              SizedBox(
                height: 100,
                child: AutoSizeText(
                  S.of(context).selection_of_account_aggregator,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.roboto(color: Colors.white, fontSize: 20),
                ),
              ),
              SizedBox(height: 20),
              Container(
                margin: EdgeInsets.all(20),
                color: Colors.white,
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    ListTile(
                      leading: Image.asset('assets/images/perfious_logo.png',
                          width: 100, height: 100, fit: BoxFit.contain),
                      trailing: Checkbox(value: false, onChanged: null),
                    ),
                    ListTile(
                      leading: Image.asset('assets/images/om_logo.png',
                          width: 100, height: 100, fit: BoxFit.contain),
                      trailing: Checkbox(
                          value: selectedAA,
                          onChanged: (selected) {
                            setState(() {
                              selectedAA = !selectedAA;
                            });
                          }),
                    ),
                    ListTile(
                      leading: Image.asset('assets/images/finvu_logo.png',
                          width: 100, height: 100, fit: BoxFit.contain),
                      trailing: Checkbox(value: false, onChanged: null),
                    ),
                    ListTile(
                      leading: Image.asset('assets/images/yodlee_logo.png',
                          width: 100, height: 100, fit: BoxFit.contain),
                      trailing: Checkbox(value: false, onChanged: null),
                    ),
                    ListTile(
                      leading: Image.asset('assets/images/nadl_logo.png',
                          width: 100, height: 100, fit: BoxFit.contain),
                      trailing: Checkbox(value: false, onChanged: null),
                    ),
                  ],
                ),
              ),
              MaksPayWidgets.buildButton(
                  context, '/onemoney', S.of(context).c_continue)
            ]),
          ),
        ));
  }
}
