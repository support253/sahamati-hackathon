import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../theme.dart';

class MaksPayWidgets {
  static buildButton(BuildContext context, String navRoute, String buttonText,
      {Color color, Color bgColor}) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: SizedBox(
        width: MediaQuery.of(context).size.width - 100,
        height: 50,
        child: RaisedButton(
          shape: StadiumBorder(),
          color: bgColor ?? MaksPayColors.secondaryMaks,
          onPressed: () {
            Navigator.of(context).pushNamed(navRoute);
          },
          child: Text(buttonText,
              style: GoogleFonts.roboto(
                  color: color ?? MaksPayColors.primaryMaks,
                  fontWeight: FontWeight.w700)),
        ),
      ),
    );
  }
}
