import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:makspay/generated/l10n.dart';
import 'package:makspay/theme.dart';
import 'package:velocity_x/velocity_x.dart';

class LoanOffersCard extends StatelessWidget {
  final String bankName;
  final String bankImage;
  final double bankImageScale;
  final double payBy;
  final double offeredLoan;
  final double pA;
  final double interest;
  final VoidCallback onTap;
  const LoanOffersCard({
    Key key,
    this.bankName,
    this.bankImage,
    this.bankImageScale,
    this.payBy,
    this.offeredLoan,
    this.pA,
    this.interest,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        elevation: 5,
        child: Container(
          width: context.screenWidth,
          height: 80,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  SizedBox(
                    width: 8,
                  ),
                  Image.asset(
                    bankImage,
                    scale: bankImageScale,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(bankName),
                  SizedBox(
                    width: 8,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("₹ $offeredLoan"),
                      SizedBox(
                        height: 10,
                        width: 50,
                        child: AutoSizeText(
                          S.of(context).loan,
                          style: context.textTheme.caption,
                          minFontSize: 9,
                          stepGranularity: 3,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text("+"),
                  SizedBox(
                    width: 8,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("₹ $interest"),
                      SizedBox(
                        height: 10,
                        width: 50,
                        child: AutoSizeText(
                          "$pA% ${S.of(context).p_a}",
                          style: context.textTheme.caption,
                          minFontSize: 9,
                          stepGranularity: 3,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: MaksPayColors.primaryMaks,
                  )
                ],
              ),
              SizedBox(
                height: 12,
              ),
              Text(
//                          "Pay Rs. 2514.24 by 30 Aug 2020 (30 days)",
                S.of(context).pay_by(payBy),
                textAlign: TextAlign.center,
                style: context.textTheme.caption
                    .copyWith(color: MaksPayColors.primaryMaks),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
