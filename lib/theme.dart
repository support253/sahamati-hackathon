import 'package:flutter/material.dart';

class MaksPayColors {
  static Color primaryMaks = Color(0xff02302E);
  static Color secondaryMaks = Color(0xff34EADC);
  static Color white = Color(0xffffffff);
}
